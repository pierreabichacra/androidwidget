package com.example.weatherapp;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.location.LocationManager;
import android.media.Image;
import android.util.Log;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.example.weatherapp.api.weather.WeatherApiManager;
import com.example.weatherapp.models.CurrentWeatherInfo;
import com.example.weatherapp.models.WeatherDescription;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import java.io.IOException;
import java.util.Timer;
import java.util.TimerTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Implementation of App Widget functionality.
 * App Widget Configuration implemented in {@link WeatherAppWidgetConfigureActivity WeatherAppWidgetConfigureActivity}
 */
public class WeatherAppWidget extends AppWidgetProvider {


    static void updateAppWidget(Context context, AppWidgetManager appWidgetManager,
                                int appWidgetId) {

        CharSequence widgetText = WeatherAppWidgetConfigureActivity.loadTitlePref(context, appWidgetId);
        // Construct the RemoteViews object
     final   RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.weather_app_widget);
        final Context context1 = context;
        final AppWidgetManager appWidgetManager1 = appWidgetManager;
        final int appWidgetId1 = appWidgetId;

        views.setTextViewText(R.id.appwidget_text, widgetText);
        WeatherApiManager weatherApiManager = new WeatherApiManager();


        weatherApiManager.getWeatherByCityName("beirut").enqueue(new Callback<CurrentWeatherInfo>() {
            @Override
            public void onResponse(Call<CurrentWeatherInfo> call, Response<CurrentWeatherInfo> response) {

                CurrentWeatherInfo currentWeatherInfo = response.body();
                Log.d("myLog", currentWeatherInfo.getTempInfo().toString());
                String temp = String.valueOf(currentWeatherInfo.getTempInfo().getTemp());
                views.setTextViewText(R.id.widgetCurrentTemp, temp);


                    WeatherDescription currentWeatherDescription = currentWeatherInfo.getWeatherDescriptions().get(0);

                    String weatherDescription = currentWeatherDescription.getDescription();
                    String iconUrl = "http://openweathermap.org/img/w/" + currentWeatherDescription.getIcon() + ".png";

                    Picasso.with(context1)
                            .load(iconUrl)
                            .into(new Target() {
                                @Override
                                public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                                    views.setImageViewBitmap(R.id.widgetCurrentTempImage,bitmap);
                                    appWidgetManager1.updateAppWidget(appWidgetId1, views);
                                }

                                @Override
                                public void onBitmapFailed(Drawable errorDrawable) {

                                }

                                @Override
                                public void onPrepareLoad(Drawable placeHolderDrawable) {

                                }
                            });



                appWidgetManager1.updateAppWidget(appWidgetId1, views);

            }

            @Override
            public void onFailure(Call<CurrentWeatherInfo> call, Throwable t) {

            }
        });

        // Instruct the widget manager to update the widget
        appWidgetManager.updateAppWidget(appWidgetId, views);

    }

    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        // There may be multiple widgets active, so update all of them
        for (int appWidgetId : appWidgetIds) {

            updateAppWidget(context, appWidgetManager, appWidgetId);
        }



    }



    //==============================================================================================
   //==============================================================================================


    @Override
    public void onDeleted(Context context, int[] appWidgetIds) {
        // When the user deletes the widget, delete the preference associated with it.
        for (int appWidgetId : appWidgetIds) {
            WeatherAppWidgetConfigureActivity.deleteTitlePref(context, appWidgetId);
        }
    }

    @Override
    public void onEnabled(Context context) {
        // Enter relevant functionality for when the first widget is created
    }

    @Override
    public void onDisabled(Context context) {
        // Enter relevant functionality for when the last widget is disabled
    }
}

